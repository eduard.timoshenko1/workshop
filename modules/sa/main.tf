terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
      version = "0.57.0"
    }
  }
}

resource "yandex_iam_service_account" "instances" {
  name = "${var.env}-account"
  description = "service account for manage VMs"
  folder_id = var.folder_id
}

resource "yandex_resourcemanager_folder_iam_binding" "editor" {
  folder_id = var.folder_id
  members = [
    "serviceAccount:${yandex_iam_service_account.instances.id}"
  ]
  role = "editor"
  depends_on = [
    yandex_iam_service_account.instances
  ]
}