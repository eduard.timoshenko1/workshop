terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
      version = "0.57.0"
    }
  }
}

resource "yandex_kubernetes_cluster" "kub" {
  name  = "kub-${var.env}"
  network_id = var.network_id
  node_service_account_id = var.service_account_id
  service_account_id = var.service_account_id
  release_channel = var.release_chan
  network_policy_provider = var.network_provider
  master {
    zonal {
      zone = var.zone
      subnet_id = var.yandex_vpc_subnet_id
    }
    version = var.ver
    public_ip = true
  }
}