resource "helm_release" "helm" {
  chart = var.chart_name
  name = var.chart_name
  repository = var.chart_repo
  namespace = var.chart_namespace
  version = var.ver
  atomic = true
  force_update = false
  create_namespace = true
  values = var.values
  depends_on = [var.depends]
}